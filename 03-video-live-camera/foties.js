var cap;
var mask2;
var mirror;
var video1;
var x, y;
var poem = [];
var i=0;
var countdown = 2000;
var cnv;
var myFont;
let items = [];
let msec  = 500; //every 500ms. 
let count = 0;


function preload() {

  mask2 = loadImage('data/insidemirror.png');
  mirror = loadImage('data/mirror3.png');
  video1 = createVideo(['data/fire.mp4']);
  myFont = loadFont('data/PFGaramondClassic-OsFReg.ttf');
  poem = loadStrings('data/fires_text.txt', doText);
  stars = loadImage ("data/stars.png");
  starscopy = loadImage ("data/stars-copy.png");
}

function doText(data) {
  poem = data;
}


function setup() {

  cnv = createCanvas(1280, 720);


  imageMode(CENTER);
  image(stars, windowWidth/2, windowHeight/2);
  cap = createCapture(VIDEO);
  cap.hide();
  image(starscopy, windowWidth/2, windowHeight/2);

  video1.hide();
  video1.volume(0);

  noStroke();

  setInterval(nextLine, 2000);
}


function draw() {


  if (i >= poem.length + 3) {
    background (255);
    image(stars, windowWidth/2, windowHeight/2);
    image(video1, windowWidth/2, windowHeight/2, 854, 480);
    cap.mask(mask2);

    image(cap, windowWidth/2, windowHeight/2-175, mask2.width/2, mask2.height/2);
    //image(mask2, windowWidth/2, windowHeight/5, mask2.width/2, mask2.height/2);
    //image(mirror, windowWidth/2, windowHeight/2-140, mirror.width/2, mirror.height/2);
    image(starscopy, windowWidth/2, windowHeight/2);
    text(countdown, windowWidth/2  - 400, windowHeight/2- 210);

    if (keyIsPressed === true) {

      if (keyCode === UP_ARROW && countdown > 0) {
        video1.loop();
        //countdown += 50;
      }
    } else {

      // continue, or stop
      if (countdown > 0) countdown--;
    }
  } 

  if ( countdown == 0) {
    background (255);
    image(stars, windowWidth/2, windowHeight/2);
    image(starscopy, windowWidth/2, windowHeight/2);
    //fill('#00A99D');
    textSize(16);
    textAlign(CENTER);
    //text('Ο ΧΡΟΝΟΣ ΕΙΝΑΙ ΚΟΙΝΩΝΙΚΗ ΚΑΤΑΣΚΕΥΗ', windowWidth/2, windowHeight/2);
    //input = createInput('Ο ΧΡΟΝΟΣ ΕΙΝΑΙ ΚΟΙΝΩΝΙΚΗ ΚΑΤΑΣΚΕΥΗ');
    // button = createButton("submit");
    setInterval(add_item, 1000);
    for (let i = 0; i < items.length; i++) {  
      background (255);
      image(stars, windowWidth/2, windowHeight/2);
      image(starscopy, windowWidth/2, windowHeight/2);
      // Draw all items0
      push();
      translate(items[i].left, items[i].top);
      fill(0, 169, 158);
      text('ο χρόνος είναι κοινωνική κατασκευή', 0, 0);
      pop();
    }
    // Remove all invisible items to increse performance
  }
}

function nextLine() {


  //fill(255, 153, 153);
  fill('#00A99D');
  textSize(16);
  textFont(myFont);
  text(poem[i], windowWidth/4, 20*i+windowHeight/2);
  textAlign(LEFT, BASELINE);
  i ++;
  if (i == poem.length + 1) {
    background (255);
    image(stars, windowWidth/2, windowHeight/2);
    image(starscopy, windowWidth/2, windowHeight/2);
    fill('#00A99D');
    //fill(255, 153, 153);
    textSize(16);
    textFont(myFont);
    text('Eίσαι μόνη σου.', windowWidth/4, windowHeight/2);
    textAlign(LEFT, TOP);
  }
  if (i == poem.length + 2) {
    background (255);
    image(stars, windowWidth/2, windowHeight/2);
    image(starscopy, windowWidth/2, windowHeight/2);
    //fill(255, 153, 153);
    fill('#00A99D');
    textSize(16);
    textFont(myFont);
    text('Οδήγα.', windowWidth/4, windowHeight/2);
    textAlign(LEFT, TOP);
  }
}

function add_item() {
  items.push( {
  size: 
    20, 
    opacity: 
    255, 
    left: 
    random(windowWidth), 
    top: 
    random(windowHeight)
  }
  );
  count++;
}


function keyReleased() {
  video1.pause();
}


function doubleClicked() {
  let fs = fullscreen();
  fullscreen(!fs);
}
