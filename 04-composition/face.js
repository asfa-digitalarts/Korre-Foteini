let stars;
let blue2;
let input;
let blue3;
let threshold = 0.01;
let l = 255;

function preload() {

  blue2 = loadImage ("images/blue2.jpg");
  blue3 = loadImage ("images/blue2.jpg");
  stars = loadImage ("images/stars.png");
}


function setup() {

  createCanvas(1280, 720);
  imageMode(CENTER);
  pixelDensity(1);
  blue2.resize(width, height);
  blue3.resize(width, height);

  input = new p5.AudioIn();
  input.start();
}


function draw() {

  blue2.loadPixels();

  const newPixels = [];

  let volume = input.getLevel();
  console.log(input.getLevel());

  // Loop through the pixels
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {

      // Get the index for our current pixel
      const index = (y * width + x) * 4;

      // And the index for one pixel to the right
      let nextIndex = (y * width + x) * 4 + (4 * int(random(-2, 2)));
      nextIndex = nextIndex % (width * height * 4);

      const r = blue2.pixels[nextIndex + 0];
      const g = blue2.pixels[nextIndex + 1];
      const b = blue2.pixels[nextIndex + 2];

      newPixels.push(r, g, b, 255);
    }
  }

  for (let i = 0; i < newPixels.length; i++) {
    blue2.pixels[i] = newPixels[i];
  }

  // We're finished working with pixels
  blue2.updatePixels();

  if ( volume > threshold) {
    l = l-500*volume;
  } 
  else if(volume < threshold && l<255){ 
    l = l + 5;
  }

  push();
  tint(255, l);
  // Draw the images
  image(blue2, windowWidth/2, windowHeight/2);
  pop();
  push();
  tint(255, 255-l);
  image(blue3, windowWidth/2, windowHeight/2);
  pop();
  image(stars, windowWidth/2, windowHeight/2, 1280, 720);
}
